﻿using System;

namespace BowlingGame2
{
   public class Game
    {
        private int[] pinsFallen = new int[21];
        private int rollCount;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public void Roll(int pins)
        {
            pinsFallen[rollCount] = pins;
            rollCount++;
        }
        public int Score()
        {
            int i = 0;
            int score = 0;
            for (int frame = 0; frame < 10;frame++)
            {
                if(pinsFallen[i]==10)
                {
                    score += 10 + pinsFallen[i + 1] + pinsFallen[i + 2];
                    i += 1;
                }
                else if(pinsFallen[i]+pinsFallen[i+1]==10)
                {
                    score += 10 + pinsFallen[i + 2];
                    i += 1;
                }
                else 
                {
                    score += pinsFallen[i] + pinsFallen[i + 1];
                    i += 2;
                }
            }
            return score;
        }
    }
}
